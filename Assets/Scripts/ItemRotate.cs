﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotate : MonoBehaviour
{
    public float RotateSpeed;

    private void Update()
    {
        transform.Rotate(0, RotateSpeed, 0, Space.World);
    }
}
